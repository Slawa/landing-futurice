$(function(){

    // auto height for first screen
    var $window = $(window),
        updateHeight = function(){
            var height = $window.height();

            if (height >= 835 ){
                height = 835;
            }
            if (height <= 500){
                height = 500;
            };

            $('.js-full-height').css({
                height: height
            })
        };

    $window.resize(function(){
        updateHeight();
    });
    updateHeight();


    // sliders for company
    $('.js-b-slider').length && $('.js-b-slider').slick({
        dots: true,
        focusOnSelect: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: $('.b-slider__prev.first'),
        nextArrow: $('.b-slider__next.first')
    });

    $('.js-b-slider2').length && $('.js-b-slider2').slick({
        dots: true,
        focusOnSelect: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: $('.b-slider__prev.second'),
        nextArrow: $('.b-slider__next.second')
    });

    $('.js-company-slider').length && $('.js-company-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        variableWidth: true,
        arrows: false,
        height: 225
    });

    $('.js-office-slider').length && $('.js-office-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        variableWidth: true,
        arrows: false,
        height: 225
    });

    // custom select
    var $select = $('#select-country').first();
    $select.length && $select.select2();


    // loading effect
    $('.p-loading').addClass('p-loading_state_hide');


    // check email plugin
    function CheckEmail(select){
        var self = this;

        self.errors = {
            empty: 'Email is empty',
            incorrect: 'Email is incorrect'
        };

        self.params = {
            $select: $(select),
            $form: $(select).closest('form'),
            form: $(select).closest('form')[0],
            $errorLabel : $(select).closest('form').find('.js-input-error').first(),
            errorClass: 'show_error_email',
            emailReg: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        };

        self.isShowError = false;

        self.params.$form
            .on('blur', select, function(e){
                e.preventDefault;
                self.checkInput();
            })
            .on('change keypress', select, function(e){
                e.preventDefault;
                self.hideError();
            })
    }

    CheckEmail.prototype.showError = function(){
        var self = this;

        if (self.isShowError) return;

        self.isShowError = true;
        self.params.$form.addClass(self.params.errorClass);
    };

    CheckEmail.prototype.hideError = function(){
        var self = this;

        if (!self.isShowError) return;

        self.isShowError = false;
        self.params.$form.removeClass(self.params.errorClass);
    };

    CheckEmail.prototype.checkInput = function(){
        var self = this,
            value = $.trim(self.params.$select.val());

        if (!value){
            self.params.$errorLabel.text(
                self.errors.empty
            );
            self.showError();
            return false;
        }

        if (!self.params.emailReg.test(value)){
            self.params.$errorLabel.text(
                self.errors.incorrect
            );
            self.showError();
            return false;
        }

        return true;
    };

    $('.f-subs').length && $('.f-subs').data('checkEmail', new CheckEmail('.js-check-email'));

    $('body')
        .on('click', '.js-submit-subs', function (e) {
            var checkEmail = $('.f-subs').data('checkEmail');

            if (!checkEmail.checkInput()) {
                e.preventDefault();
            } else {
                $('.footer-subs').toggleClass('footer-subs_show_again');
            };
        })
        .on('click', '.js-subscribe-again', function(e){
            $('.footer-subs').toggleClass('footer-subs_show_again');

            $('.js-check-email').val('');
            return false;
        });


    // select + input for form
    var $selectCountry = $('.js-select-country'),
        $inputCountry = $('.js-input-country');

    $selectCountry.on('select2:select', function(){
        $inputCountry.val($selectCountry.find('option:selected').text());
    });

});